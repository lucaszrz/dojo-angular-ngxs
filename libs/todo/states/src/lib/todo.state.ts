import { Todo } from '@ngxs-todo/todo/models';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { TodoService } from '@ngxs-todo/todo/services';
import {
  AddTodo,
  DeleteTodo,
  GetTodos,
  SetSelectedTodo,
  UpdateTodo,
} from '@ngxs-todo/todo/actions';
import { tap } from 'rxjs';
import { Injectable } from '@angular/core';

export interface TodoStateModel {
  todos: Todo[];
  selectedTodo: Todo | null;
}

@State<TodoStateModel>({
  name: 'todos',
  defaults: {
    todos: [],
    selectedTodo: null,
  },
})
@Injectable()
export class TodoState {
  constructor(private todoService: TodoService) {}

  @Selector()
  static getTodoList(state: TodoStateModel) {
    return state.todos;
  }

  @Selector()
  static getSelectedTodo(state: TodoStateModel) {
    return state.selectedTodo;
  }

  @Action(GetTodos)
  getTodos({ getState, setState }: StateContext<TodoStateModel>) {
    return this.todoService.fetchTodos().pipe(
      tap((result) => {
        const state = getState();

        setState({
          ...state,
          todos: result,
        });
      })
    );
  }

  @Action(AddTodo)
  addTodos(
    { getState, patchState }: StateContext<TodoStateModel>,
    { payload }: AddTodo
  ) {
    return this.todoService.addTodo(payload).pipe(
      tap((result) => {
        const state = getState();

        patchState({
          todos: [...state.todos, result],
        });
      })
    );
  }

  @Action(UpdateTodo)
  updateTodos(
    { getState, setState }: StateContext<TodoStateModel>,
    { payload, id }: UpdateTodo
  ) {
    return this.todoService.updateTodo(payload, id).pipe(
      tap((result) => {
        const state = getState();
        const todoList = [...state.todos];
        const todoIndex = todoList.findIndex((todo) => todo.id === id);
        todoList[todoIndex] = result;

        setState({
          ...state,
          todos: todoList,
        });
      })
    );
  }

  @Action(DeleteTodo)
  deleteTodo(
    { getState, setState }: StateContext<TodoStateModel>,
    { id }: DeleteTodo
  ) {
    return this.todoService.deleteTodo(id).pipe(
      tap(() => {
        const state = getState();
        const filteredTodoList = state.todos.filter((todo) => todo.id !== id);

        setState({
          ...state,
          todos: filteredTodoList,
        });
      })
    );
  }

  @Action(SetSelectedTodo)
  setSelectedTodo(
    { getState, setState }: StateContext<TodoStateModel>,
    { payload }: SetSelectedTodo
  ) {
    const state = getState();

    setState({
      ...state,
      selectedTodo: payload,
    });
  }
}
