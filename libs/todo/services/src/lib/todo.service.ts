import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Todo } from '@ngxs-todo/todo/models';

const BASE_URL = 'https://jsonplaceholder.typicode.com/todos';

@Injectable({
  providedIn: 'root',
})
export class TodoService {
  constructor(private http: HttpClient) {}

  fetchTodos() {
    return this.http.get<Todo[]>(BASE_URL);
  }

  deleteTodo(id: number) {
    return this.http.delete(`${BASE_URL}/${id}`);
  }

  addTodo(payload: Todo) {
    return this.http.post<Todo>(BASE_URL, payload);
  }

  updateTodo(payload: Todo, id: number) {
    return this.http.put<Todo>(`${BASE_URL}/${id}`, payload);
  }
}
