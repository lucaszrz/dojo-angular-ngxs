import { Component } from '@angular/core';

@Component({
  selector: 'ngxs-todo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'todo';
}
