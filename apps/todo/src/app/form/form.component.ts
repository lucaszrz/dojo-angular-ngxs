import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Todo } from '@ngxs-todo/todo/models';
import { TodoState } from '@ngxs-todo/todo/states';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { AddTodo, SetSelectedTodo, UpdateTodo } from '@ngxs-todo/todo/actions';

interface ITodoForm {
  id: FormControl<number>;
  userId: FormControl<number>;
  title: FormControl<string>;
}

@Component({
  selector: 'ngxs-todo-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  @Select(TodoState.getSelectedTodo) selectedTodo!: Observable<Todo>;

  todoForm: FormGroup<ITodoForm>;

  isEditing = false;

  private formSubscription: Subscription = new Subscription();

  constructor(private store: Store) {
    this.todoForm = new FormGroup<ITodoForm>({
      id: new FormControl(),
      userId: new FormControl(0, {
        validators: [Validators.required],
        nonNullable: true,
      }),
      title: new FormControl('', {
        validators: [Validators.required],
        nonNullable: true,
      }),
    });
  }

  ngOnInit(): void {
    this.formSubscription.add(
      this.selectedTodo.subscribe((todo) => {
        if (todo) {
          this.todoForm.patchValue(todo);
          this.isEditing = true;
        } else {
          this.todoForm.reset();
          this.isEditing = false;
        }
      })
    );
  }

  onSubmit() {
    if (this.isEditing) {
      this.formSubscription.add(
        this.store
          .dispatch(
            new UpdateTodo(
              this.todoForm.getRawValue() as Todo,
              this.todoForm.getRawValue().id
            )
          )
          .subscribe(() => {
            this.cleanForm();
          })
      );
    } else {
      this.formSubscription.add(
        (this.formSubscription = this.store
          .dispatch(new AddTodo(this.todoForm.getRawValue() as Todo))
          .subscribe(() => {
            this.cleanForm();
          }))
      );
    }
  }

  cleanForm() {
    this.todoForm.reset();
    this.store.dispatch(new SetSelectedTodo(null));
  }
}
